//
//  IdentifyViewController.swift
//  FaceRecV2
//
//  Created by Nattawat Kanmarawanich on 27/8/2564 BE.
//  www.youtube.com/watch?v=OxKHt1NwOHw&t=306s

import UIKit
import CoreML
import Vision

class IdentifyViewController: UIViewController {

    @IBOutlet weak var imgBtn: UIButton!
    @IBOutlet weak var identifyBtn: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var bufferView: UIImageView!
    
    var imagePicker = UIImagePickerController()
//    let iOSModel = iOS_Team_1()
    var tempImg : UIImage!

    override func viewDidLoad() {
        super.viewDidLoad()
//        let AonImg = UIImage(named: "Aon")
//        imgView.image = AonImg
//        tempImg = AonImg
//        identifyClick()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnClicked() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
        
    }
    
    @IBAction func identifyClick() {
        if let idenImg1 = tempImg {
        
//            let imgsize = idenImg1.getSizeIn(.kilobyte)
//            idenImg.resizingMode
            let resizeCG = CGSize(width: 224, height: 224)
//            let resizeCG = CGSize(width: 299, height: 299)
            let idenImg = idenImg1.resizeImage(image: idenImg1, targetSize: resizeCG)
            
//            let pixelBuffer = ImageProcessor.pixelBuffer(forImage: idenImg.cgImage!)
            let pixelBuffer = buffer(from: idenImg!)
            
            
            if let pixelBuffer = pixelBuffer {
//              let image = UIImage(ciImage: CIImage(cvPixelBuffer: pixelBuffer))
//              bufferView.image = image
              }
            
//            guard let model = try? VNCoreMLModel(for: IOS2().model)else {
//                fatalError("Unable to load model")
//            }
//
//            let coreMlRequest = VNCoreMLRequest(model: model) {[weak self] request, error in
//                    guard let results = request.results as? [VNClassificationObservation],
//                        let topResult = results.first
//                        else {
//                            fatalError("Unexpected results")
//                    }
//                    DispatchQueue.main.async {[weak self] in
//                        self?.nameLabel.text = topResult.identifier
//                    }
//                }
//
//
//            let handler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer!, options: [:])
//               DispatchQueue.global().async {
//                   do {
//                       try handler.perform([coreMlRequest])
//                   } catch {
//                       print(error)
//                   }
//               }
            
            
            
            
//              let model = try? SqueezeNet()
        
//              let model = try? IOS2(configuration: MLModelConfiguration())
            let model = try? MobileNetV2(configuration: MLModelConfiguration())
              do {
                let name = try model?.prediction(image: pixelBuffer!)

                if name != nil{
                  self.nameLabel.text = name?.classLabel

                } else {
                    self.nameLabel.text = "Unknown Person"
                }
              } catch let error {
                print(error)
              }
           
        }
        
//
       
    }
}

extension IdentifyViewController : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if let img = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")]as? UIImage
        {
            imgView.image = img
            tempImg = imgView.image
        }
        
        
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func buffer(from image: UIImage) -> CVPixelBuffer? {
      let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
      var pixelBuffer : CVPixelBuffer?
      let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(image.size.width), Int(image.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
      guard (status == kCVReturnSuccess) else {
        return nil
      }

      CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
      let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)

      let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
      let context = CGContext(data: pixelData, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)

      context?.translateBy(x: 0, y: image.size.height)
      context?.scaleBy(x: 1.0, y: -1.0)

      UIGraphicsPushContext(context!)
      image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
      UIGraphicsPopContext()
      CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))

        
            
    
      return pixelBuffer
    }
    
}

extension UIImage {
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
//        let size = image.size
        
//        let widthRatio  = targetSize.width  / size.width
//        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
//        if(widthRatio > heightRatio) {
//            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
//        } else {
//            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
//        }
        newSize = targetSize
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: newSize)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }

    public enum DataUnits: String {
        case byte, kilobyte, megabyte, gigabyte
    }

    func getSizeIn(_ type: DataUnits)-> String {

        guard let data = self.pngData() else {
            return ""
        }

        var size: Double = 0.0

        switch type {
        case .byte:
            size = Double(data.count)
        case .kilobyte:
            size = Double(data.count) / 1024
        case .megabyte:
            size = Double(data.count) / 1024 / 1024
        case .gigabyte:
            size = Double(data.count) / 1024 / 1024 / 1024
        }

        return String(format: "%.2f", size)
    }
}


//
////            let config = MLModelConfiguration()
////            let iOSModel = try? photoClassifier_1(configuration: config)
//              let iOSModel = photoClassifier_1()
////            guard let name = try? iOSModel?.prediction(image: pixelBuffer!) else {fatalError("Unexpected Runtime Error")}
////            let input = try iOSModel(sceneImage: pixelBuffer)
////            let output = try? iOSModel.prediction(image: pixelBuffer)
//
////            let input = iOS_Team_1Input(image: pixelBuffer!)
////            let name = try? iOSModel.prediction(image: pixelBuffer!)
////
////
////            if name != nil{
////                self.nameLabel.text = name?.classLabel
////            }
////            else{
////                self.nameLabel.text = "Unknown Person"
////            }
////
//
////            do{
////                let config = MLModelConfiguration()
////                let iOSModel = try? iOS_Team_1(configuration: config)
////                let input = iOS_Team_1Input(image: pixelBuffer!)
////
////                let output = try? iOSModel!.prediction(input: input)
////
////                self.nameLabel.text = output?.classLabel
////
////            }
